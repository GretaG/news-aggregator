@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a New Feed</div>

                    <div class="card-body">
                        <form method="POST" action="/feeds">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="url">Feed url</label>
                                <input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}">
                            </div>
                            <div class="form-group">
                                <label for="url">Provider url</label>
                                <input type="text" class="form-control" id="provider_url" name="provider_url" value="{{ old('provider_url') }}">
                            </div>
                            <div class="form-group">
                                <label for="category_id">Chose a category</label>
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value="">-</option>
                                    @foreach($categories as $category)
                                        <option {{ (old('category_id') == $category->id) ? "selected='selected'" : "" }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Publish</button>
                            </div>
                            @if(count($errors))
                                <ul class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection