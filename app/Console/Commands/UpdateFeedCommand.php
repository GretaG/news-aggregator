<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-09
 * Time: 13:12
 */

namespace App\Console\Commands;

use App\Services\RssService;
use Illuminate\Console\Command;

class UpdateFeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates news from feeds';

    /**
     * @var RssService
     */
    protected $rss;

    /**
     * UpdateFeedCommand constructor.
     *
     * @param RssService $rss
     */
    public function __construct(RssService $rss)
    {
        parent::__construct();

        $this->rss = $rss;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->rss->provideData();

        $this->info("Feed is updated");
    }
}