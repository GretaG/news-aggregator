$(document).ready(function () {
    $(document).on("click", ".news-title", function () {
        var title = $(this).attr("data-title");
        var description = $(this).attr("data-description");
        var link = $(this).attr("data-link");
        $('#myNewsModal .modal-title').html(title);
        $('#myNewsModal .modal-body').html(description);
        $('#myNewsModal .news-page').attr("href", link);
    });
});
