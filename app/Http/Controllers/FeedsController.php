<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feed;
use Illuminate\Http\Request;

class FeedsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param null $categorySlug
     * @return \Illuminate\Http\Response
     */
    public function index($categorySlug = null)
    {
        if ($categorySlug) {
            $feeds = Category::where('slug', $categorySlug)->first()->feeds()->get();
        } else {
            $feeds = Feed::latest()->get();
        }

        return view('feeds.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('feeds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'url' => 'required|url',
            'provider_url' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ]);

        $feed = Feed::create([
            'title' => request('title'),
            'url' => request('url'),
            'provider_url' => request('provider_url'),
            'category_id' => request('category_id')
        ]);

        return redirect(route('feed_edit', ['feed' => $feed->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        return view('feeds.edit', ['feed' => $feed]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
        $feed->update(request()->validate([
            'title' => 'required',
            'url' => 'required|url',
            'provider_url' => 'required|url',
            'category_id' => 'required|exists:categories,id'
        ]));

        return redirect(route('feed_edit', ['feed' => $feed->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
        $feed->delete();

        return redirect(route('feeds_index'));
    }

}
