<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-11
 * Time: 00:03
 */

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Feed;

class FeedsTest extends TestCase
{
    use DatabaseMigrations;

    public function testUserCanNotSeeFeeds()
    {
        $this->withExceptionHandling();

        $this->get('/feeds')
            ->assertRedirect(route('login'));
    }

    public function testAdminCanSeeFeeds()
    {
        $this->signIn();

        $this->get('/feeds')
            ->assertStatus(200);
    }

    function testUserCanNotCreateFeeds()
    {
        $this->withExceptionHandling();

        $this->get('/feeds/create')
            ->assertRedirect(route('login'));

        $this->post(route('feeds_index'))
            ->assertRedirect(route('login'));
    }

    function testUserCanNotDeleteFeeds()
    {
        $this->withExceptionHandling();

        $feed = create('App\Feed', [
            'title' => 'Some Title',
            'url' => 'www.google.com',
            'provider_url' => 'www.google.com',
            'category_id' => 1,
        ]);

        $this->delete($feed->path())->assertRedirect('/login');
    }

    function testAdminCanDeleteFeeds()
    {
        $this->withExceptionHandling();

        $feed = create('App\Feed', [
            'title' => 'Some Title',
            'url' => 'www.google.com',
            'provider_url' => 'www.google.com',
            'category_id' => 1,
        ]);

        $this->signIn();
        $this->delete($feed->path())->assertRedirect(route('feeds_index'));
        $this->assertNull(Feed::where('id', $feed->id)->first());
    }

    function testFeedRequiresATitle()
    {
        $this->publishFeed(['title' => null])
            ->assertSessionHasErrors('title');
    }

    function testFeedRequiresAUrl()
    {
        $this->publishFeed(['url' => null])
            ->assertSessionHasErrors('url');
    }

    function testFeedRequiresAValidCategory()
    {
        factory('App\Category', 2)->create();

        $this->publishFeed(['category_id' => null])
            ->assertSessionHasErrors('category_id');
    }

    protected function publishFeed($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $feed = make('App\Feed', $overrides);

        return $this->post(route('feeds_index'), $feed->toArray());
    }
}