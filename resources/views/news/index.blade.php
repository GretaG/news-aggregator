@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach ($news as $oneNews)
                    <div class="card mb-4">
                        <div class="card-header">
                            <div class="d-inline-block">
                                <h4 class="news-title" style="cursor: pointer" data-toggle="modal" data-target="#myNewsModal"
                                    data-title='{{ $oneNews->title }}' data-link='{{ $oneNews->url }}'
                                    data-description='{{ $oneNews->description }}'>
                                    {{ $oneNews->title }}
                                </h4>
                                <a href="{{ $oneNews->provider_url }}" target="_blank">
                                    {{ $oneNews->provider_url }}
                                </a>
                            </div>

                        </div>
                        <div class="card-body">
                            <div>{!! $oneNews->description !!}</div>
                        </div>
                    </div>
                @endforeach
                <div class="modal fade" id="myNewsModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"></h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <a class="news-page" href="">News page</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection