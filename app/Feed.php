<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url', 'provider_url', 'category_id'];

    /**
     * Return feed path
     * @return string
     */
    public function path()
    {
        return '/feeds/' . $this->id;
    }

}
