<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    public function feeds()
    {
        return $this->hasMany(Feed::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }
}
