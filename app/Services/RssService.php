<?php
/**
 * Created by PhpStorm.
 * User: Kubas
 * Date: 2019-02-08
 * Time: 00:04
 */

namespace App\Services;

use App\Feed;
use App\News;
use Illuminate\Support\Facades\Validator;

class RssService
{
    /**
     * Provides news
     */
    public function provideData()
    {
        $this->markAsOldNews();

        $rssFeeds = $this->simpleXmlReader();

        $this->saveAllNews($rssFeeds);

    }

    /**
     * Marks current news as old
     */
    public function markAsOldNews()
    {
        $news = News::where('old_news', 0)->get();

        foreach ($news as $oneNews) {
            $oneNews->old_news = 1;
            $oneNews->save();
        }
    }

    /**
     * Scans the xlm file, returns data array
     *
     * @return array
     */
    public function simpleXmlReader(): array
    {
        $feeds = Feed::all();
        $rssFeeds = array();

        if ($feeds) {
            foreach ($feeds as $feed) {
                if(file_exists($feed->url)) {
                    if (($rssXml = simplexml_load_file($feed->url)) !== false) {
                        $rssFeeds[] = array(
                            'rss_xml' => $rssXml,
                            'category_id' => $feed->category_id,
                            'provider_url' => $feed->provider_url
                        );
                    }
                }
            }
        }

        return $rssFeeds;
    }

    /**
     * Process and saves all news
     *
     * @param array $rssFeeds
     */
    public function saveAllNews(array $rssFeeds)
    {
        foreach ($rssFeeds as $rssFeed) {
            foreach ($rssFeed['rss_xml']->channel->item as $item) {
                $itemArray = array(
                    'title' => $item->title,
                    'description' => $item->description,
                    'url' => $item->link,
                    'provider_url' => $rssFeed['provider_url'],
                    'category_id' => $rssFeed['category_id']
                );

                $this->createOneNews($itemArray);
            }
        }
    }

    /**
     * Creates one news in db
     *
     * @param array $itemArray
     * @return bool
     */
    public function createOneNews(array $itemArray)
    {
        $validator = Validator::make($itemArray, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required|url',
            'provider_url' => 'required|url',
            'category_id' => 'required|exists:categories,id',
        ]);

        if ($validator->passes()) {
            News::create([
                'title' => $itemArray['title'],
                'description' => $itemArray['description'],
                'url' => $itemArray['url'],
                'provider_url' => $itemArray['provider_url'],
                'category_id' => $itemArray['category_id']
            ]);
        } else {
            return false;
        }
    }
}