<?php

namespace Tests\Feature;

use Tests\TestCase;

class NewsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanSeeNews()
    {
        $response = $this->get('/news');

        $response->assertStatus(200);
    }

    public function testCanNotCreateNews()
    {
        $this->withExceptionHandling();

        $this->get('/news/create')
            ->assertStatus(500);
    }

}
