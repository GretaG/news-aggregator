<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news', 'NewsController@index')->name('news_index');
Route::get('/news/{category}', 'NewsController@index')->name('news_index_category');

Route::middleware(['auth'])->group(function () {
    Route::middleware(['can:isAdmin'])->group(function () {
        Route::get('/feeds', 'FeedsController@index')->name('feeds_index');
        Route::post('/feeds', 'FeedsController@store')->name('feeds_store');
        Route::get('/feeds/create', 'FeedsController@create')->name('feeds_create');
        Route::delete('/feeds/{feed}', 'FeedsController@destroy')->name('feed_delete');
        Route::get('/feeds/{feed}/edit', 'FeedsController@edit')->name('feed_edit');
        Route::patch('/feeds/{feed}', 'FeedsController@update')->name('feed_update');
        Route::get('/feeds/{category}', 'FeedsController@index')->name('feeds_index_category');

        Route::get('/categories/create', 'CategoriesController@create')->name('categories_create');
        Route::post('/categories', 'CategoriesController@store')->name('categories_store');

    });
});


