#### Setup

Clone the repository onto your computer.<br>
Create new .env file using example '.env.example'<br>
Run commands<br>

```
>   composer install
```

```
>   npm install
```

```
>   npm run dev
```

or

```
>   npm run watch
```

and

```
>   php artisan migrate
```


### Create admin user
```
>   php artisan user:create {name} {email} {password}
```

### Update feeds
```
>   php artisan feed:update
```

### Run tests

```
> vendor/bin/phpunit
```