@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach ($feeds as $feed)
                    <div class="card mb-4">
                        <div class="card-header">
                            <div class="d-inline-block">
                                <h4>
                                    <a href="{{ route('feed_edit', ['feed' => $feed->id ]) }}">
                                        {{ $feed->title }}
                                    </a>
                                </h4>
                            </div>
                            <div class="d-inline-block float-right">
                                <form action="/feeds/{{ $feed->id }}" method="POST">
                                    {!! method_field('DELETE') !!}
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-link">Delete</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div>{{ $feed->url }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection