<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Services\RssService;
use App\News;
use App\Feed;
use App\Category;

class RssServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $rss;

    protected function setUp()
    {
        parent::setUp();

        $this->rss = new RssService;
    }

    public function testMarkCurrentNewsAsOldNews()
    {
        factory('App\News')->create();

        $this->rss->markAsOldNews();

        $news = News::first()->old_news;

        $this->assertEquals(1, $news);
    }

    public function testOrSimpleXmlReaderReadsDataFromXmlFile()
    {
        Feed::create([
            'title' => "Lietuva",
            'url' => 'tests/Helpers/lithuania.xml',
            'provider_url' => "https://www.delfi.lt",
            'category_id' => 1
        ]);

        $rssFeeds = $this->rss->simpleXmlReader();

        $this->assertCount(3, $rssFeeds[0]);
        $this->assertCount(2, $rssFeeds[0]["rss_xml"]->channel->item);
        $this->assertEquals(1, $rssFeeds[0]["category_id"]);
        $this->assertEquals("https://www.delfi.lt", $rssFeeds[0]["provider_url"]);
    }

    public function testOrOneNewsIsSaved()
    {
        Category::create([
            'name' => "Category name",
            'slug' => "Category slug",
        ]);

        $itemArray = array(
            "title" => "First item title",
            "description" => "First item description",
            "url" => "https://www.delfi.lt",
            "provider_url" => "https://www.delfi.lt",
            "category_id" => 1,
        );

        $this->rss->createOneNews($itemArray);

        $this->assertCount(1, News::all());
        $this->assertEquals("First item title", News::first()->title);
    }

    public function testOrOneNewsSaveReturnsFalseOnNonValidData()
    {
        Category::create([
            'name' => "Category name",
            'slug' => "Category slug",
        ]);

        $itemArray = array(
            "title" => "First item title",
            "description" => "First item description",
            "url" => "First item url",
            "provider_url" => "https://www.delfi.lt",
            "category_id" => 1,
        );

        $valid = $this->rss->createOneNews($itemArray);

        $this->assertFalse($valid);
    }


}
